const options = {
  apiKey: 'a786ce0148f062550833d3681135f7b5085a95bf1b7a704f21ce2494ad401f4e',
  username: 'mydeliverysms' 
}
const AfricasTalking = require('africastalking')(options)
const sms = AfricasTalking.SMS

module.exports = app => {
  app.post('/sms', (req, res) => {
    let data = req.body
    let providers = {}

    for (let key in data.items) {
      if (data.items.hasOwnProperty(key)) {
        let providerPhoneNumber = data.items[key].providerPhoneNumber
        let product = {
          amount: data.items[key].amount,
          name: data.items[key].name
        }
        providers[providerPhoneNumber] = providers[providerPhoneNumber] || {}
        providers[providerPhoneNumber][key] = product
      }
    }

    let smsPromises = []

    for (let providerPhoneNumber in providers) {
      if (providers.hasOwnProperty(providerPhoneNumber)) {
        let providerData = providers[providerPhoneNumber]
        let products = []
        for (let key in providerData) {
          if (providerData.hasOwnProperty(key)) {
            products.push('ITEM: ' + providerData[key].name + ' AMOUNT: ' + providerData[key].amount)
          }
        }
        message = products.join('\n') + '\nTo: ' + data.location.area + ', ' + data.location.building + '\nCustomer Phone Number: ' + data.userphoneNumber
        smsPromises.push(sms.send({ to: providerPhoneNumber, message: message, enqueue: true }))
      }
    }
    Promise.all(smsPromises)
      .then(
        res.status(200).send()
      )
      .catch(error => {
        console.log('Error while sending SMS:', error)
      })
  })
}