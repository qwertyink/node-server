const express = require ('express')

const app = express()
const PORT = process.env.PORT || 8081

require('./config/express')(app)

require('./config/routes')(app)

app.listen(PORT, () => {
  console.log('App running on port:' + PORT)
})
