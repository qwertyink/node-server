# Introduction
This is the server that handles the back-end functionality for myDelivary.com such as SMS and M-Pesa transactions.

# Getting Started
To get started with this server:
1.	Clone the repo
2.	Install node dependencies with npm or yarn
3.	Run the server locally using ``` node server.js ```

# Contribute
To contribute add a branch make changes and improvements and send a pull request when done
